#!/bin/bash

usage() {
    echo "Usage: $0 <command>"
    echo
    echo "Available commands:"
    echo
    echo "install-web-sso                           - install PostgreSQL database and 2Element web server"
    echo "install-fullstack                         - install PostgreSQL database, 2Element web server and 2Element authproxy"
    echo "install-authproxy                         - install standalone 2Element authproxy"
    echo "update                                    - update 2Element application"
    echo "psql                                      - runs PostgreSQL console for default database container"
    echo "tailf                                     - runs docker-compose logs -f command"
    echo

    exit 1
}

function detect_os() {
    if grep -qs "CentOS Linux release 8" /etc/redhat-release; then
        echo "rhel8"

        return
    fi

    if grep -qs "Red Hat Enterprise Linux release 8" /etc/redhat-release; then
        echo "rhel8"

        return
    fi

    if grep -qs "Rocky Linux release 8" /etc/redhat-release; then
        echo "rhel8"

        return
    fi

    if grep -qs "CentOS Stream release 8" /etc/redhat-release; then
        echo "rhel8"

        return
    fi

    if grep -qs "VERSION_ID=\"20.04\"" /etc/os-release; then
        echo "ubuntu2004"

        return
    fi

    echo "unknown"

    return
}

function install_prerequisites_rhel8() {
    if ! yum update -y; then
        install_failed "yum update failed"
    fi

    if ! yum install -y yum-utils; then
        install_failed "yum-utils installation failed"
    fi

    if ! yum install -y rng-tools; then
        install_failed "rng-tools installation failed"
    fi

    if ! systemctl enable --now rngd; then
        install_failed "cannot enable and start rngd service"
    fi

    if ! yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo; then
        install_failed "cannot add docker repository"
    fi

    if ! yum install -y docker-ce docker-ce-cli containerd.io; then
        install_failed "cannot install docker"
    fi

    if ! test -f /etc/docker/daemon.json; then
        echo -e "{\n  \"log-driver\": \"local\",\n  \"log-opts\": {\n    \"max-size\": \"100m\",\n    \"max-file\": \"10\",\n    \"compress\": \"true\"\n  }\n}\n" > /etc/docker/daemon.json
    fi

    sestatus=`sestatus | grep status | awk '{ print $3 }'`

    if [ "$sestatus" != "disabled" ]; then
        if ! sed -i 's/SELINUX=enforcing/SELINUX=disabled/' /etc/selinux/config; then
            install_failed "cannot configure SELinux"
        fi

        if ! setenforce 0; then
            install_failed "cannot disable SELinux"
        fi
    fi

    if ! systemctl enable --now docker; then
        install_failed "cannot enable docker service"
    fi

    if ! curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose; then
        install_failed "cannot download docker-compose"
    fi

    if ! chmod +x /usr/local/bin/docker-compose; then
        install_failed "cannot set docker-compose as executable"
    fi

    if ! yum install -y git; then
        install_failed "cannot install git"
    fi
}

function install_prerequisites_ubuntu2004() {
    if ! sudo apt update; then
        install_failed "apt update failed"
    fi

    if ! sudo apt upgrade -y; then
        install_failed "apt upgrade failed"
    fi

    if ! sudo apt-get install rng-tools -y; then
        install_failed "rng-tools installation failed"
    fi

    if ! sudo apt-get install -y apt-transport-https ca-certificates curl gnupg lsb-release; then
        install_failed "apt utils installation failed"
    fi

    if ! curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --yes --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg; then
        install_failed "cannot add docker repository key"
    fi

    if ! echo "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null; then
        install_failed "cannot add docker repository"
    fi

    if ! sudo apt-get update; then
        install_failed "cannot run apt-get update"
    fi

    if ! sudo apt-get install -y docker-ce docker-ce-cli containerd.io; then
        install_failed "cannot install docker"
    fi

    if ! test -f /etc/docker/daemon.json; then
        echo -e "{\n  \"log-driver\": \"local\",\n  \"log-opts\": {\n    \"max-size\": \"100m\",\n    \"max-file\": \"10\",\n    \"compress\": \"true\"\n  }\n}\n" > /etc/docker/daemon.json
    fi

    if ! sudo systemctl enable --now docker; then
        install_failed "cannot enable docker service"
    fi

    if ! curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose; then
        install_failed "cannot download docker-compose"
    fi

    if ! chmod +x /usr/local/bin/docker-compose; then
        install_failed "cannot set docker-compose as executable"
    fi

    if ! sudo apt-get install -y git; then
        install_failed "cannot install git"
    fi
}

function install_and_run_2element() {
    local profile=$1

    if [ "$profile" != "fullstack" ] && [ "$profile" != "web-sso" ] && [ "$profile" != "authproxy" ]; then
      install_failed "unsupported installation profile $profile"
    fi

    if [ ! -d "element2-install" ]; then
        if ! git clone https://bitbucket.org/sonpo/element2-install.git; then
            install_failed "cannot download installation files"
        fi
    fi

    if ! cd element2-install; then
        install_failed "installation files not found"
    fi

    if [ ! -f docker-compose.yml ]; then
        cp "docker-compose.yml.example-${profile}" docker-compose.yml
    fi

    if [ ! -f docker-compose.override.yml ]; then
        cp "docker-compose.override.yml.example-${profile}" docker-compose.override.yml

        replace_default_password_in_docker_compose_override_file
    fi

    if ! docker network inspect 2element >/dev/null 2>&1; then
        if ! docker network create 2element; then
            install_failed "cannot create 2element docker network"
        fi
    fi

    if ! /usr/local/bin/docker-compose pull; then
        install_failed "cannot pull docker images"
    fi

    if [ "$profile" == "authproxy" ]; then
        echo -e ""
        echo -e "-----------------------"
        echo -e "Post-installation notes"
        echo -e "-----------------------"
        echo -e ""
        echo -e "Please edit docker-compose.override.yml file and run:"
        echo -e ""
        echo -e "/usr/local/bin/docker-compose up -d"
        echo -e ""
    else
        if ! /usr/local/bin/docker-compose up -d; then
            install_failed "cannot start docker containers"
        fi
    fi
}

function install_failed() {
    local message=$1

    echo "Installation failed: $message"

    exit 1
}

function install() {
    local os=$(detect_os)
    local profile=$1

    case "$os" in
        "rhel8")
            install_prerequisites_rhel8
            ;;
        "ubuntu2004")
            install_prerequisites_ubuntu2004
            ;;
        *)
            install_failed "unsupported OS"
    esac

    install_and_run_2element "${profile}"
}

function update() {
    if ! /usr/local/bin/docker-compose pull; then
        install_failed "cannot pull docker images"
    fi

    if ! /usr/local/bin/docker-compose up -d; then
        install_failed "cannot start docker containers"
    fi
}

function psql() {
    if ! docker exec -it postgres psql -U postgres element2; then
        echo "Cannot run PostgreSQL console"

        exit 1
    fi
}

function tailf() {
    if ! /usr/local/bin/docker-compose logs -f --tail 100; then
        echo "Cannot run docker-compose"

        exit 1
    fi
}

function generate_random_password() {
    < /dev/urandom tr -dc _A-Z-a-z-0-9 | head -c ${1:-16}; echo
}

function replace_default_password_in_docker_compose_override_file() {
    local password=$(generate_random_password)

    sed -i "s/    - POSTGRES_PASSWORD=pleaseSetPassword$/    - POSTGRES_PASSWORD=$password/" docker-compose.override.yml
    sed -i "s/    - JDBC_PASSWORD=pleaseSetPassword$/    - JDBC_PASSWORD=$password/" docker-compose.override.yml
}

command=$1

if [ "$#" -lt 1 ]; then
  usage
fi

case $command in
    "install-fullstack")
        install "fullstack"
        ;;
    "install-web-sso")
        install "web-sso"
        ;;
    "install-authproxy")
        install "authproxy"
        ;;
    "update")
	    update
	    ;;
    "psql")
        psql
        ;;
    "tailf")
        tailf
        ;;
    *)
        usage
        ;;
esac
