#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
    CREATE DATABASE "element2";
    \c "element2";
    CREATE EXTENSION pgcrypto;
EOSQL

