# Installation

Automated installation is supported on following distributions:

* CentOS 8
* Ubuntu 20.04

Installation script will install Docker CE, docker-compose and 2Element application.

If you already have working docker installation, you can install 2Element manually with *docker-compose* by downloading or cloning installation repository from:

https://bitbucket.org/sonpo/element2-install/src/master/

In case of manual installation, please check the content of installer.sh script for necessary pre-install steps.

## Web SSO installation

To perform Web SSO install that contains PostgreSQL database and 2Element web server, run this command as root:

```
curl -s https://bitbucket.org/sonpo/element2-install/raw/master/installer.sh | bash -s install-web-sso
```

## Full-stack installation

To perform full-stack automated install that contains PostgreSQL, 2Element web server and 2Element auth-proxy, run this command as root:

```
curl -s https://bitbucket.org/sonpo/element2-install/raw/master/installer.sh | bash -s install-fullstack
```

## Standalone auth-proxy

To install standalone 2Element auth-proxy automatically, run this command as root:

```
curl -s https://bitbucket.org/sonpo/element2-install/raw/master/installer.sh | bash -s install-authproxy
```

Then edit docker-compose.override.yml file in element2-install directory and run:

```
docker-compose up -d
```

## Update

To update installed application, run this command as root in element2-install directory:

```
bash installer.sh update
```

## Data storage

In default configuration, data are stored on local disk in */var/docker-data* directory and mounted into containers using bind mounts.
